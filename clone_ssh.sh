echo "Clone WorldSynth repositories"
echo "============================="
read -p "Clone all? [y/n]: " yn
if [ "$yn" == "y" ] ; then
    clone_engine=true
    clone_opennbt=true
    clone_patcher=true
    clone_composer=true
    clone_minecraft_addon=true
    clone_forge=true
    echo "Cloning all"
elif [ "$yn" == "n" ] ; then
    clone_engine=true
    clone_opennbt=true
    clone_patcher=false
    clone_composer=false
    clone_minecraft_addon=false
    clone_forge=false
    echo "Selective clone"
else
    read -p "Press enter to exit"
    exit 1
fi

if [ "$yn" == "n" ] ; then
    echo "Engine and OpenNBT is always cloned"
    
    read -p "Clone Patcher? [y/n]: " cp
    if [ "$cp" == "y" ] ; then
        clone_patcher=true
    fi
    
    read -p "Clone Composer? [y/n]: " cc
    if [ "$cc" == "y" ] ; then
        clone_comopser=true
    fi
    
    read -p "Clone Minecraft-addon? [y/n]: " cm
    if [ "$cm" == "y" ] ; then
        clone_minecraft_addon=true
    fi
    
    read -p "Clone Forge? [y/n]: " cf
    if [ "$cf" == "y" ] ; then
        clone_forge=true
    fi
fi



## CLONE
if [ $clone_engine == true ] ; then
    echo ""
    echo "============================="
    echo "Clone Engine"
    echo "============================="
    git clone git@gitlab.com:wsynth/worldsynth-engine.git
fi

if [ $clone_opennbt == true ] ; then
    echo ""
    echo "============================="
    echo "Clone OpenNBT"
    echo "============================="
    git clone git@gitlab.com:wsynth/opennbt.git
fi

if [ $clone_patcher == true ] ; then
    echo ""
    echo "============================="
    echo "Clone Patcher"
    echo "============================="
    git clone git@gitlab.com:wsynth/worldsynth-patcher.git
fi

if [ $clone_composer == true ] ; then
    echo ""
    echo "============================="
    echo "Clone Composer"
    echo "============================="
    git clone git@gitlab.com:wsynth/worldsynth-composer.git
fi

if [ $clone_minecraft_addon == true ] ; then
    echo ""
    echo "============================="
    echo "Clone Minecraft-addon"
    echo "============================="
    git clone git@gitlab.com:wsynth/worldsynth-lib_minecraft.git
fi

if [ $clone_forge == true ] ; then
    echo ""
    echo "============================="
    echo "Clone Forge"
    echo "============================="
    git clone git@gitlab.com:wsynth/worldsynth-forge.git
fi



echo ""
read -p "Configure for eclipse? [y/n]: " conf_eclipse
configure_eclipse=false
if [ "$conf_eclipse" == "y" ] ; then
    configure_eclipse=true
fi


## GRADLE ECLIPSE
if [ $clone_engine == true ] ; then
    if [ $configure_eclipse == true ] ; then
        echo ""
        echo "============================="
        echo "Engine setup Eclipse"
        echo "============================="
        ( cd worldsynth-engine ; ./gradlew eclipse )
    fi
fi

if [ $clone_opennbt == true ] ; then
    if [ $configure_eclipse == true ] ; then
        echo ""
        echo "============================="
        echo "OpenNBT setup Eclipse"
        echo "============================="
        ( cd opennbt ; ./gradlew eclipse )
    fi
fi

if [ $clone_patcher == true ] ; then
    if [ $configure_eclipse == true ] ; then
        echo ""
        echo "============================="
        echo "Patcher setup Eclipse"
        echo "============================="
        ( cd worldsynth-patcher ; ./gradlew eclipse )
    fi
fi

if [ $clone_composer == true ] ; then
    if [ $configure_eclipse == true ] ; then
        echo ""
        echo "============================="
        echo "Composer setup Eclipse"
        echo "============================="
        ( cd worldsynth-composer ; ./gradlew eclipse )
    fi
fi

if [ $clone_minecraft_addon == true ] ; then
    if [ $configure_eclipse == true ] ; then
        echo ""
        echo "============================="
        echo "Minecraft-addon setup Eclipse"
        echo "============================="
        ( cd worldsynth-lib_minecraft ; ./gradlew eclipse )
    fi
fi

#if [ $clone_forge == true ] ; then
#	if [ $configure_eclipse == true ] ; then
#       echo ""
#       echo "============================="
#       echo "Forge setup Eclipse"
#       echo "============================="
#		./worldsynth-forge/gradlew setupDecompWorkspace
#		./worldsynth-forge/gradlew eclipse
#	fi
#fi


read -p "Press enter to finish"
